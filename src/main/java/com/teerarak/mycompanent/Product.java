/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.mycompanent;

import java.util.ArrayList;

/**
 *
 * @author AuyouknoW
 */
public class Product {
    private int id;
    private String name;
    private double Price;
    private String image;

    public Product(int id, String name, double Price, String image) {
        this.id = id;
        this.name = name;
        this.Price = Price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", Price=" + Price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Espresso 1",40,"1.jpg"));
        list.add(new Product(1,"Espresso 2",40,"2.jpg"));
        list.add(new Product(1,"Espresso 3",50,"3.jpg"));
        list.add(new Product(1,"Americano 1",40,"1.jpg"));
        list.add(new Product(1,"Americano 2",50,"2.jpg"));
        list.add(new Product(1,"Americano 3",40,"3.jpg"));
        list.add(new Product(1,"Chayen 1",45,"1.jpg"));
        list.add(new Product(1,"Chatyen 2",45,"2.jpg"));
        return list;
        
    }
}
